import { Demoday2Page } from './app.po';

describe('demoday2 App', () => {
  let page: Demoday2Page;

  beforeEach(() => {
    page = new Demoday2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
