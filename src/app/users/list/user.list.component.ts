/**
 * Created by Burakhan on 05/08/2017.
 */

import { Component } from '@angular/core';
import { UsersService } from '../service';

@Component({
  templateUrl: './user.list.component.html'
})
export class UserListComponent {

  userList = [];
  usernameFilter: string;
  userTempList = [];

  constructor(private userService: UsersService) {
  }

  ngOnInit() {
    this.userService.list().subscribe((response) => {
      this.userList = response;
      this.userTempList = response;
    });
  }

  filterUsername() {
    this.userList = this.userTempList.filter(
      (user) => {
        return user.username.indexOf(this.usernameFilter) > -1;
      }
    );
  }
}
