/**
 * Created by Burakhan on 05/08/2017.
 */

import { Injectable } from '@angular/core';
import { BaseHttpService } from '../service/_http';

@Injectable()
export class UsersService {
  constructor(private httpService: BaseHttpService) {
  }

  list() {
    return this.httpService.get('/users');
  }
}
