/**
 * Created by Burakhan on 05/08/2017.
 */
import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
const API_URL = 'http://a.idvlabs.com:3000';

@Injectable()
export class BaseHttpService {

  private withCredentialsValue: boolean = false;

  private headers: Headers = new Headers({
    'Content-Type': 'application/json',
  });

  constructor(private http: Http,) {
  }

  public get(path: string): Observable<any> {
    return this.http.get(`${API_URL}${path}`, {
      headers: this.headers,
      withCredentials: this.withCredentialsValue,
    })
      .map(this.checkForError)
      .catch((err) => this.error(err))
      .map(this.getJson)
      .share()
      ;
  }

  public post(path: string, body): Observable<any> {
    return this.http.post(
      `${API_URL}${path}`,
      JSON.stringify(body),
      {
        withCredentials: this.withCredentialsValue,
        headers: this.headers
      }
    )
      .map(this.checkForError)
      .catch((err) => Observable.throw(err))
      .map(this.getJson);
  }

  public put(path: string, body): Observable<any> {
    return this.http.put(
      `${API_URL}${path}`,
      JSON.stringify(body),
      {
        withCredentials: this.withCredentialsValue,
        headers: this.headers
      }
    )
      .map(this.checkForError)
      .catch((err) => Observable.throw(err))
      .map(this.getJson);
  }

  public delete(path: string): Observable<any> {
    return this.http.delete(
      `${API_URL}${path}`,
      {headers: this.headers}
    )
      .map(this.checkForError)
      .catch((err) => Observable.throw(err))
      .map(this.getJson);
  }

  private getJson(response: Response) {
    return response.json();
  }

  private checkForError(response: Response): Response {
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      let error = new Error(response.statusText);
      error['response'] = response;
      throw error;
    }
  }

  private error(err) {
    console.error(err);
    return null;
  }


}