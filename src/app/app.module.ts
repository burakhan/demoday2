import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { UserListComponent } from './users/list/user.list.component';
import { HomeComponent } from './home.component';
import { BaseHttpService } from './service/_http';
import { UsersService } from './users/service';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'kullanici-listesi',
        component: UserListComponent
      },

    ]),
    HttpModule,
    FormsModule,
  ],
  providers: [
    BaseHttpService,
    UsersService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
